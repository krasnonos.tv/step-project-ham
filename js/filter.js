'use strict';

$(document).ready(() => {
  const btnLoadMore = $('.our-work-load-btn');

  $('.all').click((event) => {
    event.preventDefault();
    $('.portfolio-item').hide();
    $('.portfolio-item:not(.hidden)').show();
    btnLoadMore.show();
  });

  $('.graphic-design').click((event) => {
    event.preventDefault();
    $('.portfolio-item').filter('li[data-category*="graphic"]').show();
    $('.portfolio-item').filter(':not(li[data-category*="graphic"])').hide();
    btnLoadMore.hide();
  });

  $('.web-design').click((event) => {
    event.preventDefault();
    $('.portfolio-item').filter('li[data-category*="web"]').show();
    $('.portfolio-item').filter(':not(li[data-category*="web"])').hide();
    btnLoadMore.hide();
  });

  $('.landing-page').click((event) => {
    event.preventDefault();
    $('.portfolio-item').filter('li[data-category*="landing"]').show();
    $('.portfolio-item').filter(':not(li[data-category*="landing"])').hide();
    btnLoadMore.hide();
  });

  $('.wordpress').click((event) => {
    event.preventDefault();
    $('.portfolio-item').filter('li[data-category*="wordpress"]').show();
    $('.portfolio-item').filter(':not(li[data-category*="wordpress"])').hide();
    btnLoadMore.hide();
  })
});